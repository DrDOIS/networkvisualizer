# Network Visualizer
The pyhton 3 script runs nmap to scan the local network. It generates two html summaries.

## IP table
The ip table shows all devices found during scanning. It is possible to associate an alias to recognize devices based on their MAC and find out their current ip address.
It is furthermore possible to define the ip address as clickable, which generates a button in the table.
If a device is configured as 'always_on' in the aliases.json and is not found during scanning, it will be shown as a red row in the table.

![](ips.png)

## Visualisation of the network

The second html file generated is a visualisation of the network topology. vis.js is used to generate a dynamic network based on the aliases.json.
In the config file it is possible to define 'connected_to' relations, which are represented in the interactive image.

![](visu.png)

## Installation

Clone the repo (recursive). 

Requriements to run the script:
 - Python >= 3.6
 - nmap installed on the system
 - pip package 'python-nmap'
 - It may be required to run the script as root
 
## Configuration
 1. Define the ip ranges to scan in the aliases.json under key 'ranges'
 2. Add your devices under 'associations'
 
## Possible improvements
 - The topology is only based on the config and not the results of the scan. The measurements could be used to represent online/offline states in the network.