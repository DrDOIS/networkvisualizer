#  Requirements
# - python-nmap

import os
import nmap
import json
from datetime import datetime
from typing import Tuple

__DIR_ROOT = os.path.abspath(os.path.dirname(sys.modules['__main__'].__file__))
aliases_file = os.path.join(__DIR_ROOT, "aliases.json")

'''
[
    {
        "ip" : "192.168",
        "mac" : "A0:0B",
        "online" : true,66
        "alias" : "-",
        "settings" : []
    },
]
'''


def scan_hosts(hosts: str) -> dict:
    nm = nmap.PortScanner()
    scan_res = nm.scan(hosts=hosts, arguments='-n -sP -PE -PA21,23,80,3389')
    return scan_res["scan"]


def create_table2(config: list) -> str:
    header = '<table class="table table-dark table-hover">\n'
    for row in config:
        header += create_column2(row)
    header += '</table>\n'
    return header


def create_column2(config: dict) -> str:
    entry = "<tr"

    if config["online"] == False:
        # Make row red
        entry += ' class="bg-danger"'
    entry += ">"

    ip = config["ip"]
    ip_string = config["ip"]
    if "settings" in config and "clickable" in config["settings"]:
        ip_string = '<a target="_blank" rel="noopener noreferrer" href="http://' + ip + '" class="btn btn-sm btn-success">' + ip + '</a>'

    entry += '<td>' + ip_string + '</td>'
    entry += '<td>' + config["alias"] + '</td>'
    entry += '<td>' + config["mac"] + '</td>'

    entry += '</tr>'
    return entry


def parse_active_hosts(measurements: list, aliases: dict) -> list:
    active_hosts = []  # list of dicts
    '''
    [
        {
            -> "ip" : "192.168",
            -> "mac" : "A0:0B",
            -> "online" : true,
            "alias" : "-",
            "settings" : []
        },
    ]
    '''

    for content in measurements:
        if not "mac" in content[1]["addresses"]:
            continue
        mac = content[1]["addresses"]["mac"]
        entry = {}
        entry["ip"] = content[0]
        entry["mac"] = mac
        entry["online"] = True
        active_hosts.append(entry)
    return active_hosts

    '''
    [
        {
            "ip" : "192.168",
            "mac" : "A0:0B",
            "online" : true,
            -> "alias" : "-",
            -> "settings" : []
        },
    ]
    '''


def add_static_infos(active_hosts: dict, alias: dict) -> dict:
    for content in active_hosts:
        mac = content["mac"]
        if not mac in aliases["associations"]:
            # No configuration stored
            content["alias"] = "-"
            continue
        static_config = aliases["associations"][mac]
        content["alias"] = static_config["alias"]
        if "settings" in static_config:
            content["settings"] = static_config["settings"]
        else:
            content["settings"] = []

    # Add offline entries
    for mac, static_content in aliases["associations"].items():
        if not "settings" in static_content:
            continue
        if not "always_on" in static_content["settings"]:
            continue
        # Check if entry is on
        found = False
        for entry in active_hosts:
            if mac == entry["mac"]:
                found = True
                break
        if found == False:
            new_entry = {}
            new_entry["ip"] = "Missing"
            new_entry["alias"] = static_content["alias"]
            new_entry["mac"] = mac
            new_entry["online"] = False
            active_hosts.append(new_entry)

    return active_hosts


def generate_table(config: dict, aliases: dict) -> Tuple[str, list]:
    sorted_ips = sorted(config.items(), key=my_key)

    config = parse_active_hosts(sorted_ips, aliases)
    config = add_static_infos(config, aliases)

    table = create_table2(config)

    return table, config


def split_ip(ip):
    """Split a IP address given as string into a 4-tuple of integers."""
    return tuple(int(part) for part in ip.split('.'))


def my_key(item):
    return split_ip(item[0])


aliases = {}
with open(aliases_file, 'r') as f:
    aliases = json.load(f)

active_hosts = {}
for range in aliases["ranges"]:
    active_hosts.update(scan_hosts(range))

table, generated_config = generate_table(active_hosts, aliases["associations"])

table = table.replace("&lt;br/&gt;", "<br/>")  # Restore newlines)

html = ""
html += '<head>\n'
html += '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">\n'
html += '</head>\n\n'
html += '<body>\n'
html += '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>\n'
html += "Generiert am " + datetime.now().strftime('%d.%m %H:%M') + "<br>\n"
html += table + "\n"
html += '</body>'

storage_file = os.path.join(__DIR_ROOT, "ips.html")
f = open(storage_file, "w")
f.write(html)
f.close()

'''
Returns a dict with all connections (without duplications)
{
    "A0:B3" : ["C1:3B", "2D", "FG"],
    "C1:3B" : ["A0:B3", "A4"]
}
'''


def generateNodeList(static_config: dict):
    assocs = {}
    for mac, content in static_config.items():
        if "connected_to" in content:
            assocs[mac] = content["connected_to"]

    # Remove duplications
    # {
    #     "A0:B3": ["C1:3B", "2D", "FG"],
    #     "C1:3B": ["A4"]
    # }

    for router_mac, connections in assocs.items():
        for single_con in connections:
            if single_con in assocs:
                # Then the router_mac must not be in the list of single_con
                if router_mac in assocs[single_con]: assocs[single_con].remove(router_mac)

    # Generate id table
    # {
    #     "MAC": 1,
    #     "MAC2": 2,
    # }
    ids = {}
    i = 1
    group = 0
    nodes2 = []
    switches = getAllSwitches(static_config)
    for router, content in assocs.items():
        if not router in ids:
            ids[router] = i

            entry = {}
            entry["id"] = i
            entry["label"] = getAlias(router, static_config)
            entry["group"] = group
            entry["shape"] = "diamond"
            nodes2.append(entry)
            i += 1

        for content_entry in content:
            if not content_entry in ids and not content_entry in switches:
                ids[content_entry] = i

                cofig = {}
                cofig["id"] = i
                cofig["label"] = getAlias(content_entry, static_config)
                cofig["group"] = group
                cofig["shape"] = "dot"
                nodes2.append(cofig)
                i += 1
        group += 1

    # Generate nodes entry
    # nodes = []
    # for mac, id in ids.items():
    #     entry = {}
    #     entry["id"] = id
    #     entry["label"] = getAlias(mac, static_config)
    #     nodes.append(entry)

    nodes_text = json.dumps(nodes2, indent=2)

    # Generate edges (connections)
    edges = []
    for router, other_device in assocs.items():
        router_id = getId(router, ids)
        for device in other_device:
            edge = {}
            edge["from"] = router_id
            edge["to"] = getId(device, ids)
            edges.append(edge)

    edges_text = json.dumps(edges, indent=2)

    return nodes_text, edges_text


def getAllSwitches(assocs: dict) -> list:
    switches = []
    for mac, content in assocs.items():
        if "connected_to" in content:
            switches.append(mac)
    return switches

def getAlias(mac: str, static_config: dict) -> str:
    if mac in static_config and "alias" in static_config[mac]:
        return static_config[mac]["alias"]
    return mac


def getId(mac: str, ids: dict) -> str:
    return ids[mac]


# Visualisation
nodes, edges = generateNodeList(aliases["associations"])

visu = "<html>\n<head>\n"
visu += '<script type="text/javascript" src="vis/dist/vis.js"></script>\n'
visu += '<style>\n        body {\n            color: #d3d3d3;\n            font: 12pt arial;\n            background-color: #ffffff;\n        }\n\n        #mynetwork {\n        border: 1px solid #444444;\n            background-color: #dddddd;\n        }\n    </style>'
visu += '</head>\n'

visu += '<body>\n'
visu += '<div id="mynetwork"></div>\n'
visu += '<script type="text/javascript">'
visu += 'var nodes = new vis.DataSet('
visu += nodes
visu += ');\n\
\n\
  // create an array with edges\n\
  var edges = new vis.DataSet('
visu += edges
visu += ');'

visu += '  // create a network\nvar container = document.getElementById("mynetwork");\nvar data = {\nnodes: nodes,\nedges: edges\n};'
visu += ' var options = {\n\
        nodes: {\n\
            shape: "dot",\n\
            size: 30,\n\
            font: {\n\
                size: 20\n\
            },\n\
            borderWidth: 2,\n\
            shadow:true\n\
        },\n\
        edges: {\n\
            width: 2,\n\
            shadow:true,\n\
            color: {\n\
                color: "#848484"\n\
            },\n\
            length:100\n\
        }\n\
    };'

visu += 'var network = new vis.Network(container, data, options);\n</script>'
visu += '</body>'

visu_file = os.path.join(__DIR_ROOT, "visu.html")
f = open(visu_file, "w")
f.write(visu)
f.close()
